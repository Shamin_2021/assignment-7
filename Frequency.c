#include <stdio.h>
#include <string.h>
#define SIZE 100

int main() {
    char str[SIZE],letter;
    int frequency = 0,length;

    printf("Enter a the string you desire          : ");
    scanf("%[^\n]s",str);
    
    length = strlen(str);
  
    printf("Enter a character to find the frequency: ");
    scanf(" %c", &letter);

    for (int i = 0; i< length; ++i) {
        if (letter == str[i])
           frequency ++;
    }

    printf("-----------------------------------------------------\n");
    printf("Frequency of letter |%c| occuring in entered string is : %d", letter, frequency);
    return 0;
}