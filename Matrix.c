#include <stdio.h>
#define SIZE 100

int main(void) {

  int rows1,cols1,rows2,cols2;

  printf("---MATRIX 1---\n");
  printf("enter the number of rows  :");
  scanf("%d",&rows1);
  printf("enter the number of cols  :");
  scanf("%d",&cols1);
  int matrix1[rows1][cols1];

  for(int i=0;i<rows1;i++){
    for(int j=0;j<cols1;j++){
      printf("Enter row %d col %d element :",i,j);
      scanf("%d",&matrix1[i][j]);
    }
  }

  printf("---MATRIX 2---\n");
  printf("enter the number of rows  :");
  scanf("%d",&rows2);
  printf("enter the number of cols  :");
  scanf("%d",&cols2);
  int matrix2[rows2][cols2];

  for(int k=0;k<rows2;k++){
    for(int l=0;l<cols2;l++){
      printf("Enter row %d col %d element :",k,l);
      scanf("%d",&matrix2[k][l]);
    }
  }
   

  int matrix3[rows1][cols2];

  for(int i=0;i<rows1;i++){
    for(int j=0;j<cols2;j++){
      matrix3[i][j]=0;
    }
  }
  
  if(cols1 != rows2){
    printf("sorry the multiplication is not possible as matrix 1 has %d cols and matrix2 has %d rows therefore not equal\n",cols1,rows2);
  }
  else
  {
    for(int i=0;i<rows1;i++){
			for(int j=0;j<cols1;j++){
				for(int k=0;k<rows2;k++){
          matrix3[i][j] = matrix3[i][j]+matrix1[i][k]*matrix2[k][j];
      }
  }
  }
  }
  printf("\n----------------------------------\n");
	printf("Result of Matirx Multiplication:\n");
  printf("ORDER of new Matirx Multiplication:\n");

	for(int i=0;i<rows1;i++){
		for(int j=0;j<cols2;j++)
			printf("|%5d|    ", matrix3[i][j]);
		printf("\n");
	}
  return 0;
}

